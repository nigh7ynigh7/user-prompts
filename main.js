

const readLine = require('./read');

var notImpl = () => { throw new Error('Not yet implemented'); }

class Executor {
    constructor(config){
        this.config = config; 
        this.ctx = {};
    }
    context(ctx){
        this.ctx = ctx || this.ctx;
    }
    applyNext(next){
        if(!next) throw new Error('next is empty');
        this._next = next;
    }
    next(){
        return this._next;
    }
    execute(){
        notImpl();
    }
}

class Message extends Executor {
    execute(){
        var message = (this.config || {}).message;
        if(!message){
            console.log('NO MESSAGE');
        }
        else{
            console.log(message);
        }
    }
}

class Prompt extends Executor {
    execute(){
        var conf = this.config || {};
        var target = conf.target;
        if(!target){
            throw new Error('No target message found');
        }
        this.ctx[target] = readLine(conf.prompt || '');
        if(conf.converter){
            this.ctx[target] = conf.converter(this.ctx[target]);
        }
    }
}

class Conditional extends Executor {
    execute(){
        if(this.config == null){
            throw new Error('Conditional is null')
        }
        var defaultRule = this.config.default;
        if(!defaultRule){
            throw new Error('Default rule is required');
        }
        var observe = this.ctx[this.config.observe];
        if(!observe){
            return this._next = defaultRule.to;
        }
        var rules = this.config.rules || [];
        rules.forEach(rule => {
            if(rule.val == observe){
                this._next = rule.to;
            }
        });
        if(!this._next){
            this._next = defaultRule.to;
        }
    }
}

class ExecutorGroup {
    constructor(ctx = {}){
        this.group = {};
        this.context = ctx || {};
        this._first = null;
    }
    add(name, executor, next){
        if(name in this.group){
            throw new Error('name already in group');
        }
        executor.context(this.context);
        this.group[name] = executor;
        if(next){
            executor.applyNext(next);
        }
        return this;
    }
    first(name){
        this._first = name;
        return this;
    }
    start(){
        if(!this._first || !(this._first in this.group)){
            throw new Error('First does not exist or is empty.')
        }
        let current = this._first;
        while(true){
            if(!current){
                break;
            }
            let executor = this.group[current];
            if(!executor){
                throw new Error(`Executor for ${current} not defined`);
            }
            executor.execute();
            current = executor.next();
        }
    }
}

try{
    var group = new ExecutorGroup()
        .add('start', new Message({ message : 'Hello world' }), 'feeling')
        .add('feeling', new Prompt({
                target : 'feeling',
                prompt : 'How are you?',
                converter : (a)=>a.toLowerCase()
            }),
            'check'
        )
        .add('check', new Conditional({
                observe : 'feeling',
                rules : [
                    { val : 'fine', to : 'fine' },
                    { val : 'good', to : 'fine' },
                    { val : 'sad', to : 'sad' },
                    { val : 'bad', to : 'sad' }
                ],
                default : { to : 'sorry?' }
            })
        )
        .add('sorry?', new Message({message : 'Sorry... i didn\'t quite catch that.'}), 'feeling')
        .add('fine', new Message({message : 'Fine? Well then...'}), 'good bye')
        .add('sad', new Message({message : 'Sad? Oh well :('}), 'tip')
        .add('tip', new Message({message : 'Why not play a game?'}), 'good bye')
        .add('good bye', new Message({message : 'Good Bye!'}))
        .first('start')
        .start();
}
catch(err){
    console.log(err);
}
