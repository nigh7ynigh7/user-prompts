const readline = require('readline-sync');

function readLine(prompt = ''){
    if(!prompt){
        prompt = '> ';
    }
    else{
        prompt += '\n> ';        
    }
    return readline.question(`${prompt}`);
}

module.exports = readLine;